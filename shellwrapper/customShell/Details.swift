//
//  Details.swift
//  Test
//
//  Created by Konstantin's MacBook on 8/15/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import Foundation

@discardableResult
func shell(_ str_arr: [String]) -> Int32 {
    let task = Process()
    task.launchPath = "/usr/bin/env"
    task.arguments = str_arr
    task.launch()
    task.waitUntilExit()
    return task.terminationStatus
}

func makeRequestIfReadLine() -> Request? {
    if let str = readLine() {
        return Request(str: str)
    }
    else {
        return nil
    }
}

typealias MyClos = (Request) -> ()

func createDict() -> [String: MyClos] {
    
    var builtin = [String: MyClos]()
    
    builtin["help"] = { request in
        dohelp()
    }
    
    builtin["exit"] = { request in
        doexit()
    }
    
    builtin["pwd"] = { request in
        dopwd()
    }
    
    builtin["cd"] = { request in
        docd(request: request)
    }
    
    builtin["head"] = { request in
        dohead(request: request)
    }

    builtin["tail"] = { request in
        dotail(request: request)
    }
    
    builtin["grep"] = { request in
        dogrep(request: request)
    }
    
    builtin["wc"] = { request in
        dowc(request: request)
    }
    
    builtin["shell"] = { request in
        shell(request.arguments())
    }
    
    return builtin
}
