//
//  CommandRunner.swift
//  Test
//
//  Created by Konstantin's MacBook on 8/15/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import Foundation

class CommandRunner {
    var builtin: [String: MyClos]
    
    init() {
        builtin = createDict()
    }
    
    subscript(command: String) -> MyClos {
        if let clos = builtin[command] {
            return clos
        }
        else {
            return builtin["shell"]!
        }
    }
}
