//
//  main.swift
//  Test
//
//  Created by Konstantin's MacBook on 8/14/20.     
//  Copyright © 2020 macbook. All rights reserved.
//

import Foundation

// app start here
let commandRunner = CommandRunner()

while true {
    if let request = makeRequestIfReadLine() {
        commandRunner[request.command()](request)
    }
}
