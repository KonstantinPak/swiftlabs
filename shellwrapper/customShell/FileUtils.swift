//
//  FileUtils.swift
//  Test
//
//  Created by Konstantin's MacBook on 8/15/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import Foundation

func dataToLines(data: Data, fix: String = "", N: Int = 0) -> [String] {
    let str = String(decoding: data, as: UTF8.self)
    let lines = str.split(separator: "\n")
    var ten_lines:[Substring] = []
    
    if fix == "prefix" {
        ten_lines = Array(lines.prefix(N))
    }
    else if fix == "suffix" {
        ten_lines = Array(lines.suffix(N-1))
    }
    else {
        ten_lines = lines
    }
    
    return ten_lines.map {String($0)}
}

func correctLength(request: Request) -> Int {
    var N = 10
    if request.arguments().count > 2 {
        if let tempNum = Int(request.arguments()[2]) {
            if tempNum > 0 {
                N = tempNum
            }
            else {
                N = 10
            }
        }
    }
    else {
        N = 10
    }
    return N
}

func getData(contents: [String], request: Request, fileManager: FileManager) -> Data? {
    var data: Data?
    for content in contents {
       if let path = request.path() {
        if path == content {
           if let tempData = fileManager.contents(atPath: content) {
            data = tempData
            break
           }
           else {
            print("Error reading file")
            break
           }
        }
       }
   }
    
    return data
}

func printLines(lines: [String]) -> () {
    for line in lines {
        print(line)
    }
}

func execIfCorrect(contents: [String], request: Request, fileManager: FileManager, fix: String) -> () {
    let N = correctLength(request: request)
    if let data = getData(contents: contents, request: request, fileManager: fileManager) {
        let ten_lines = dataToLines(data: data, fix: fix, N: N)
        printLines(lines: ten_lines)
    }
}

func getLinesForText(lines: [String], text: String) -> [String] {
    var tempLines: [String] = []
    for line in lines {
        if line.contains(text) {
            tempLines.append(line)
        }
    }
    return tempLines
}

func linesToWords(lines: [String]) -> Int {
    let str = lines.joined()
    let words = str.components(separatedBy: " ")
    return words.count
}

func linesToChars(lines: [String]) -> Int {
    let str = lines.joined()
    let trimmed = str.trimmingCharacters(in: .whitespacesAndNewlines)
    return trimmed.count
}

