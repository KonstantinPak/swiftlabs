//
//  Request.swift
//  Test
//
//  Created by Konstantin's MacBook on 8/15/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import Foundation

class Request {
    private var arg_arr: [String] = []
    
    init(str: String) {
        arg_arr = str.components(separatedBy: " ")
    }
    
    func arguments() -> [String] {
        return arg_arr
    }
    
    func command() -> String {
        return arg_arr[0]
    }
    
    func path() -> String? {
        if arg_arr.count > 1 {
            return arg_arr[1]
        }
        else {
            return nil
        }
    }
}
