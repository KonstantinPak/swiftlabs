//
//  File.swift
//  Test
//
//  Created by Konstantin's MacBook on 8/15/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import Foundation

func dohelp() -> () {
    print("It is Konstantin's LSH")
    print("It is builtin command")
}

func doexit() -> () {
    print("Shell terminated")
    exit(0)
}

func dopwd() -> () {
    print(FileManager.default.currentDirectoryPath)
}

func docd(request: Request) -> () {
    if let path = request.path() {
        FileManager.default.changeCurrentDirectoryPath(path)
        }
}

func dohead(request: Request) -> () {
    do {
        let contents = try FileManager.default.contentsOfDirectory(atPath: FileManager.default.currentDirectoryPath)
        
        execIfCorrect(contents: contents, request: request, fileManager: FileManager.default, fix: "prefix")
        
    } catch {
        print("Handle error by calling home contents of directory")
    }
}

func dotail(request: Request) -> () {
    do {
       let contents = try FileManager.default.contentsOfDirectory(atPath: FileManager.default.currentDirectoryPath)
       execIfCorrect(contents: contents, request: request, fileManager: FileManager.default, fix: "suffix")
   } catch {
       print("Handle error by calling home contents of directory")
   }
}

func dogrep(request: Request) -> () {
    do {
        let contents = try FileManager.default.contentsOfDirectory(atPath: FileManager.default.currentDirectoryPath)
        if let _ = request.path() {
            if request.arguments().count > 2 {
                let text = request.arguments()[2]
                if let data = getData(contents: contents, request: request, fileManager: FileManager.default) {
                    let lines = dataToLines(data: data)
                    printLines(lines: getLinesForText(lines: lines, text: text))
                }
                else {
                    print("Error reading file")
                }
            }
        }
    } catch {
        print("Handle error by calling home contents of directory")
    }
}

func dowc(request: Request) -> () {
    do {
        let contents = try FileManager.default.contentsOfDirectory(atPath: FileManager.default.currentDirectoryPath)
        if request.arguments().count > 1 {
            if let data = getData(contents: contents, request: request, fileManager: FileManager.default) {
                let lines = dataToLines(data: data)
                let words = linesToWords(lines: lines)
                let chars = linesToChars(lines: lines)
                print("Words: \(words) Chars: \(chars)")
            }
            else {
                print("Error reading file")
            }
        }
    } catch {
        print("Handle error by calling home contents of directory")
    }
}

